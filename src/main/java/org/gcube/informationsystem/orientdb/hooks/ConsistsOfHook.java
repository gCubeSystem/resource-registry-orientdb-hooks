/**
 * 
 */
package org.gcube.informationsystem.orientdb.hooks;

import org.gcube.informationsystem.model.impl.properties.PropagationConstraintImpl;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint.AddConstraint;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint.RemoveConstraint;
import org.gcube.informationsystem.model.reference.relations.ConsistsOf;
import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;

import com.orientechnologies.orient.core.db.document.ODatabaseDocument;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ConsistsOfHook extends RelationHook {

	public static PropagationConstraint propagationConstraint;

	static {
		propagationConstraint = new PropagationConstraintImpl();
		propagationConstraint.setRemoveConstraint(RemoveConstraint.cascadeWhenOrphan);
		propagationConstraint.setAddConstraint(AddConstraint.propagate);
	}

	public ConsistsOfHook() {
		super(ConsistsOf.NAME, propagationConstraint);
	}

	public ConsistsOfHook(ODatabaseDocument database) {
		super(database, IsRelatedTo.NAME, propagationConstraint);
	}

}