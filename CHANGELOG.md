This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for Resource Registry OrientDB Hooks


## [v3.0.0-SNAPSHOT] [r5.0.0] - 

- Switched JSON management to gcube-jackson [#19116]


## [v2.0.0] [r4.21.0] - 2020-03-30

- Refactored code to support IS Model reorganization (e.g naming, packages)
- Refactored code to support renaming of Embedded class to Property [#13274]
- Creating uber-jar instead of jar-with-dependencies [#10167]


## [v1.4.0] [r4.13.0] - 2018-11-20

- Using resource-registry v2 APIs [#11949]


## [v1.3.0] [r4.9.0] - 2017-12-20

- Added modifiedBy in Header [#9999]


## [v1.2.0] [r4.3.0] - 2017-03-16

- Added Propagation Constraint management on relations


## [v1.1.0] [r4.2.0] - 2016-12-15

- Improved Hook to support creation-time and lastUpdateTime management in Header


## [v1.0.1] [r4.1.0] - 2016-11-07

- Improved code quality


## [v1.0.0] [r4.0.0] - 2016-07-28

- First Release

